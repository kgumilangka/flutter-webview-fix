package io.flutter.plugins.webviewflutter;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.Manifest;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import androidx.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import androidx.core.content.FileProvider;
import java.util.*;
import java.text.SimpleDateFormat;
import android.os.Environment;
import java.lang.Math;
import android.os.StrictMode;

public class newActivity extends Activity {
    private static ValueCallback<Uri[]> mUploadMessageArray;

    public static void getfilePathCallback(ValueCallback<Uri[]> filePathCallback){
        mUploadMessageArray = filePathCallback;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.layout);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        checkPermission();
    }

    private void checkPermission(){
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.i("PERMISSION_GRANTED", "false");
            ActivityCompat.requestPermissions(
                    this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 104);
        } else {
            Log.i("PERMISSION_GRANTED", "true");
            showBottomDialog();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {

            case 104:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    showBottomDialog();
                }
                break;

            default:
                break;
        }
    }

    private void openAblum() {
//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);//任意类型文件
//        intent.setType("*/*");
//        intent.addCategory(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(intent,1);
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    private void openCarem(){
//        Intent openCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); //系统常量， 启动相机的关键
//        startActivityForResult(openCameraIntent, 2); // 参数常量为自定义的request code, 在取返回结果时有用
        dispatchTakePictureIntent();
    }

    Uri photoURI;
    private void dispatchTakePictureIntent() {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        Log.i("TAG","dispatchTakePictureIntent");
//        Log.i("TAG",takePictureIntent.resolveActivity(getPackageManager()).toString());
//        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            Log.i("TAG","resolveActivity");
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
                Log.i("TAG","createImageFile");
            } catch (IOException ex) {
                Log.i("TAG","!createImageFile");
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Log.i("TAG","photoFile");

                File storageDir = new File(
                        Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_DCIM
                        ).toString() + File.separator + "Capture"
                );
                Boolean success;
                if (!storageDir.exists()) {
                    success = storageDir.mkdir();
                    if (success) {
                        // Do something on success
                        Log.v("Crea /BottiglieDiVino", "Ho Creato nuova directory");
                    } else {
                        // Do something else on failure
                        Log.v("Crea /BottiglieDiVino", "ERRORE nel creare nuova directory");
                    }
                }
                File image = new File(storageDir, System.currentTimeMillis() + ".jpg");
                photoURI = Uri.fromFile(image);

//                photoURI = FileProvider.getUriForFile(this,
//                        "com.example.android.fileprovider",
//                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 2);
            }
//        }
    }

    String currentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }



    private void showBottomDialog(){
        //1、使用Dialog、设置style
        final Dialog dialog = new Dialog(this, R.style.DialogTheme);
        //2、设置布局
        View view = View.inflate(this, R.layout.dialog_custom_layout,null);
        dialog.setContentView(view);
        //点击其他空白处，退出dialog。
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                //这样可以使返回值为null。
                onActivityResult(1,1,null);
            }
        });
        Window window = dialog.getWindow();
        //设置弹出位置
        window.setGravity(Gravity.BOTTOM);
        //设置弹出动画
        window.setWindowAnimations(R.style.main_menu_animStyle);
        //设置对话框大小
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);

        dialog.show();

        dialog.findViewById(R.id.tv_take_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openCarem();
            }
        });

        dialog.findViewById(R.id.tv_take_pic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                openAblum();
            }
        });

        dialog.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                onActivityResult(1,1,null);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //防止退出时，data没有数据，导致闪退。
        Log.i("TAG","forResult");
        if(data != null){
            Uri uri = data.getData();
            Log.i("TAG","! "+data.getClass()+" * "+data);
            Log.i("TAG","URi "+uri);

            if(uri==null){
                //好像时部分机型会出现的问题，我的mix3就遇到了。
                //拍照返回的时候uri为空，但是data里有inline-data。
                Log.i("TAG","CAMERA ");
                Log.i("TAG URI","URi "+uri);
                Log.i("TAG", String.valueOf(data));
                Bundle bundle = data.getExtras();
                try {
//                    getContentResolver().takePersistableUriPermission(photoURI, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    Log.i("TAG", "PHOTO HIRES ");
                    Log.i("TAG URI", "URi " + photoURI);
                    Uri[] results = new Uri[]{photoURI};
                    mUploadMessageArray.onReceiveValue(results);
//                    Bitmap bitmap = (Bitmap) bundle.get("data");
//                    uri = Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, null,null));
//                    Uri[] results = new Uri[]{uri};
//                    mUploadMessageArray.onReceiveValue(results);
                }catch (Exception e){
                    //当不拍照返回相机时，获取到uri也没数据。
                    Log.i("TAG", "PHOTO HIRES ERROR");
                    mUploadMessageArray.onReceiveValue(null);
                }
            }else{
                if (requestCode == 1) {
                    getContentResolver().takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    Log.i("TAG", "GALERY ");
                    Log.i("TAG URI", "URi " + uri);
                    Uri[] results = new Uri[]{uri};
                    mUploadMessageArray.onReceiveValue(results);
                }
                else {
                    getContentResolver().takePersistableUriPermission(photoURI, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    Log.i("TAG", "PHOTO HIRES ");
                    Log.i("TAG URI", "URi " + photoURI);
                    Uri[] results = new Uri[]{photoURI};
                    mUploadMessageArray.onReceiveValue(results);
                }
            }

        }else{
            Log.i("TAG","onReceveValue");
            Log.i("TAG", "PHOTO HIRES ELSE");
            Log.i("TAG URI", "URi " + photoURI);
            Uri[] results = new Uri[]{photoURI};
            mUploadMessageArray.onReceiveValue(results);
//            mUploadMessageArray.onReceiveValue(null);
        }
        finish();
    }


}
